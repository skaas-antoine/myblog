class Category < ApplicationRecord
  has_many :blog_posts, dependent: :destroy
  validates :name, presence: true, length: { maximum: 64 }

end
