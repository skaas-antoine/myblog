class BlogPost < ApplicationRecord
  belongs_to :user
  belongs_to :category
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :title, presence: true, length: { maximum: 256 }
  validates :summary, presence: true, length: { maximum: 256}
  validates :body, presence: true

end
