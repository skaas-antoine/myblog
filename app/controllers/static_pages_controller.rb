class StaticPagesController < ApplicationController
  def home
    @blog_post = current_user.blog_posts.build if logged_in?
  end

  def help
  end

  def about
  end

  def contact
  end
end
