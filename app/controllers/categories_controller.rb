class CategoriesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def create
  end

  def destroy
  end

  def index
    @categories = Category.paginate(page: params[:page])
  end

  def show
    @category = Category.find(params[:id])
    @blog_posts = @category.blog_posts.paginate(page: params[:page])
  end
end
