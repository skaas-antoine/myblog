class BlogPostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def create
    if !current_user.writer
      flash[:danger] = "You cant post blogpost if you do not have writer rights !"
      redirect_to root_url
    end
    @blog_post = current_user.blog_posts.build(blog_post_params)
    if @blog_post.category.nil?
      @blog_post.category = Category.first
    end
    if @blog_post.save
      flash[:success] = "BlogPost created!"
      redirect_to root_url
    else
      flash[:danger] = "Something happened, please contact the dev."
      render 'static_pages/home'
    end
  end

  def destroy
    BlogPost.find(params[:id]).destroy
    flash[:success] = "BlogPost deleted"
    redirect_to request.referrer || root_url
  end

  def new
    puts "new de BC"
    @blog_post = current_user.blog_posts.build if logged_in?
    @categories = Category.all
  end

  def search
    @blog_posts = BlogPost.all
    if params[:search]
      @blog_posts = BlogPost.search(params[:search]).order("created_at DESC")
    else
      @blog_posts = nil
    end
  end

  private

    def blog_post_params
      params.require(:blog_post).permit(:title, :tags, :summary, :body)
    end
end
