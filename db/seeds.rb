# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
BlogPost.destroy_all

news = Category.create(name: "News")
films = Category.create(name: "Films")

admin = User.create!(name:  "Admin",
             email: "admin@admin.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             moderator: true,
             reader: true,
             writer: true,
             activated: true,
             activated_at: Time.zone.now)

BlogPost.create(
    title: 'Project still not finished !',
    summary: 'What an unpleasant truth',
    body: 'This is the body of the article',
    tags: 'project, blog, not finished',
    user: admin,
    category: news
)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  user = User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
  BlogPost.create(
      title: 'Sample post',
      summary: 'This is a sample seed post',
      body: 'Lorem ipsum dolor amet sit...',
      tags: 'test 1, test2',
      user: user,
      category: films
  )
end