class CreateBlogPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_posts do |t|
      t.string :title
      t.string :summary
      t.string :body
      t.string :tags, array: true
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
    add_index :blog_posts, [:user_id, :category_id, :created_at]
  end
end
